function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1)); % 4x3

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1)); % 4x5

% Setup some useful variables
m = size(X, 1); %16
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% (part 1) vectorized cost function
% size(X) = 16x2 / size(y) = 16x1 / Theta1 = 4x3 / Theta2 = 4x5 /
% num_labels = 4
% Always don't forget to put the bias unit in the front
to_add = ones(m, 1);
a1 = cat(2, to_add, X);
z2 = a1 * transpose(Theta1); % 16x4
a2 = sigmoid(z2);
a2 = cat(2, to_add, a2); % 16x5
z3 = a2 * transpose(Theta2); % 16x4
a3 = sigmoid(z3);
hThetaX = a3;

yVec = zeros(m, num_labels); %16x4
for i = 1:m
    yVec(i, y(i)) = 1;
end

J = sum(-1 * yVec .* log(hThetaX)-(1-yVec) .* log(1-hThetaX));
J = sum(J);
J = J/m;
[row_t1, col_t1] = size(Theta1);
[row_t2, col_t2] = size(Theta2);
b1 = (Theta1(:, 2:col_t1)) .* (Theta1(:, 2:col_t1));
b1 = sum(sum(b1));
b2 = (Theta2(:, 2:col_t2)) .* (Theta2(:, 2:col_t2));
b2 = sum(sum(b2));
reg = b1+b2;
reg = reg * lambda;
reg = reg/(2*m);
J = J + reg;

% -------------------------------------------------------------
% (part 2) Backpropagation
for t = 1:m
    % 1: Feedforward pass
    a1 = X(t, :); % t-th training example
    a1 = [1; transpose(a1)]; % add the bias unit 3x1
    z2 = Theta1 * a1; % 4x1
    a2 = sigmoid(z2);
    a2 = [1; a2];
    z3 = Theta2 * a2; % 4x1
    a3 = sigmoid(z3);
    % 2: back-propagate
    yk = transpose([1:num_labels]==y(t));
    d_3 = a3 - yk;
    % 3: For the hidden layer l = 2
    d_2 = transpose(Theta2) * d_3 .* [1; sigmoidGradient(z2)];
    d_2 = d_2(2:end);
    % 4: delta_1 is not calculated b/c inputs do not associate with errors
    Theta1_grad = Theta1_grad + d_2 * transpose(a1);
    Theta2_grad = Theta2_grad + d_3 * transpose(a2);
end
% =========================================================================
% 2.5
Theta1_grad = (1/m) * Theta1_grad; % 4x3
Theta1_grad = Theta1_grad + (lambda/m)*[zeros(size(Theta1, 1), 1) Theta1(:, 2:end)];
Theta2_grad = (1/m) * Theta2_grad; % 4x5
Theta2_grad = Theta2_grad + (lambda/m)*[zeros(size(Theta2, 1), 1) Theta2(:, 2:end)];
% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
