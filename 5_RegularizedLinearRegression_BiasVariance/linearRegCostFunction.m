function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

% Calculate cost
[row_t, col_t] = size(theta);
a2 = X*theta;
a2 = a2 - y;
a2 = a2 .* a2; % 10x1
a2 = sum(a2);
a2 = a2/(2*m); % 1x1
reg = theta(2:row_t, col_t) .* theta(2:row_t, col_t);
reg = sum(reg); % 1x1 
reg = (reg*lambda)/(2*m);
J = a2 + reg;

% Calculate gradient
[row_x, col_x] = size(X); %10, 3
% theta 0
grad0 = (X * theta) - y;
grad0 = transpose(X(:,1)) * grad0;
grad0 = grad0/m;
% the rest
gradRest = (X * theta) - y; % 9x1
gradRest = transpose(X(:, 2:col_x)) * gradRest; % 2x1
gradRest = gradRest/m;
to_add = theta(2:row_t, :);
to_add = to_add .* lambda;
to_add = to_add / m;
gradRest = gradRest + to_add;
grad = [grad0; gradRest];

% =========================================================================

grad = grad(:);

end
