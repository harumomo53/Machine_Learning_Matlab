function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
% x = [16x2]
m = size(X, 1); % 16
num_labels = size(Theta2, 1); %4

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1); %[16x1] FOR EACH X, PREDICT 


% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%
%size(Theta1) [4x3]
%size(Theta2) [4x5]
ones_col = ones(m, 1);
new_X = [ones_col X]; %[16x3]
a = sigmoid(Theta1 * transpose(new_X)); %[4x3]x[3x16]=[4x16]
ones_row = ones(1, m);
a = [ones_row;a]; %[5x16]
z = sigmoid(Theta2 * a); %[4x16]
for i = 1:m
    [M, I] = max(z(:,i));
    p(i) = I;
end


% =========================================================================


end
