function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Hint: The computation of the cost function and gradients can be
%       efficiently vectorized. For example, consider the computation
%
%           sigmoid(X * theta)
%
%       Each row of the resulting matrix will contain the value of the
%       prediction for that example. You can make use of this to vectorize
%       the cost function and gradient computations. 
%
% Hint: When computing the gradient of the regularized cost function, 
%       there're many possible vectorized solutions, but one solution
%       looks like:
%           grad = (unregularized gradient for logistic regression)
%           temp = theta; 
%           temp(1) = 0;   % because we don't add anything for j = 0  
%           grad = grad + YOUR_CODE_HERE (using the temp variable)
%

% vectorized the cost function (with regularization)
J = sigmoid(X * theta);
J = log(J);
J = -y .* J;
to_subtract = sigmoid(X * theta);
to_subtract = 1 - to_subtract;
to_subtract = log(to_subtract);
to_subtract = (1 - y) .* to_subtract;
J = J - to_subtract;
J = sum(J);
J = J / m;

[row_t, col_t] = size(theta);
to_add = (theta(2:row_t, col_t)).*(theta(2:row_t, col_t));
to_add = sum(to_add);
to_add = lambda * to_add;
to_add = to_add / (2 * m);
J = J + to_add;


% vectorized the gradient
[row_X, col_X] = size(X);
% theta 0 
subt1 = sigmoid(X * theta) - y; %[20x1]
multi1 = transpose(X(:, 1)) * subt1; %[1x1]
grad1 = multi1/m; %[1x1]
% anything other than theta 0
subt2 = sigmoid(X * theta) - y; % [20x1]
multi2 = transpose(X(:, 2:col_X)) * subt2; % [2x1]
grad2 = multi2/m; %[2x1]
to_add = theta(2:row_t,:); %[2x1]
to_add = to_add .* lambda;
to_add = to_add / m;
grad2 = grad2 + to_add;
grad = vertcat(grad1, grad2);
% =============================================================

grad = grad(:);

end
