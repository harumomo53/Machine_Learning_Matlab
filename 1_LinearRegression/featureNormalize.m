function [X_norm, mu, sigma] = featureNormalize(X)
%FEATURENORMALIZE Normalizes the features in X 
%   FEATURENORMALIZE(X) returns a normalized version of X where
%   the mean value of each feature is 0 and the standard deviation
%   is 1. This is often a good preprocessing step to do when
%   working with learning algorithms.

% You need to set these values correctly
X_norm = X;
mu = zeros(1, size(X, 2));
sigma = zeros(1, size(X, 2));

% ====================== YOUR CODE HERE ======================

% for each column excluding 1st one
% save the calculated mean and standard deviation
for i = 1:columns(X_norm)
	mean_i = mean(X_norm(:,i));
	mu(i) = mean_i;
	std_i = std(X_norm(:,i));
	sigma(i) = std_i;
endfor

for j = 1:rows(X_norm)
	for w = 1:columns(X_norm)
		%fprintf('X_norm(j,w) = %d\n', X_norm(j,w))
		%fprintf('mean = %d\n', mu(w))
		X_norm(j, w) = X_norm(j, w) - mu(w);
		%fprintf('X_norm(j,w) after subtraction = %d\n', X_norm(j,w))
		%fprintf('std = %d\n', sigma(w))
		X_norm(j, w) = X_norm(j, w) / sigma(w);
	endfor
endfor

% ============================================================

end
