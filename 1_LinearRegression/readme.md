Linear Regression
===
## Linear Regression with one variable
* ```ex1.m```: Octave/MATLAB script that steps you through the exercise
* ```ex1data1.txt```: Dataset for linear regression with one variable
* ```computeCost.m```: Function to compute the cost of linear regression
* ```gradientDescent.m```: Function to run gradient descent