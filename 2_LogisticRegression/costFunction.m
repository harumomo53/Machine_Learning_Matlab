function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

% Cost
J = sigmoid(X * theta);
J = log(J);
J = -y .* J;
to_subtract = sigmoid(X * theta);
to_subtract = 1 - to_subtract;
to_subtract = log(to_subtract);
to_subtract = (1 - y) .* to_subtract;
J = J - to_subtract;
J = sum(J);
J = J / m;

% Gradient 
% row, col = 3, 1
[row, col] = size(theta);
for i = 1:row
    for j = 1:col
        h = sigmoid(X * theta);
        h = h - y;
        h = h .* X(:, i);
        h = sum(h);
        h = h/m;
        grad(i, j) = h;
    end
end










% =============================================================

end
