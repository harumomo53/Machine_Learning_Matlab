function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

% Cost
J = sigmoid(X * theta);
J = log(J);
J = -y .* J;
to_subtract = sigmoid(X * theta);
to_subtract = 1 - to_subtract;
to_subtract = log(to_subtract);
to_subtract = (1 - y) .* to_subtract;
J = J - to_subtract;
J = sum(J);
J = J / m;

[row_t, col_t] = size(theta);
to_add = (theta(2:row_t, col_t)).*(theta(2:row_t, col_t));
to_add = sum(to_add);
to_add = lambda * to_add;
to_add = to_add / (2 * m);
J = J + to_add;

% Gradient
[row, col] = size(theta);
for i = 1:row
    for j = 1:col
        if i == 1
           h = sigmoid(X * theta);
            h = h - y;
            h = h .* X(:, i);
            h = sum(h);
            h = h/m;
            grad(i, j) = h;
        else
            h = sigmoid(X * theta);
            h = h - y;
            h = h .* X(:, i);
            h = sum(h);
            h = h/m;
            to_add = theta(i,:);
            to_add = to_add * lambda;
            to_add = to_add / m;
            h = h + to_add;
            grad(i, j) = h;
        end
    end
end
% =============================================================

end
